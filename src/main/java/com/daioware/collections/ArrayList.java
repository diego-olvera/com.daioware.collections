package com.daioware.collections;

import com.daioware.commons.wrapper.WrapperInt;

public class ArrayList<Data> implements AbstractList<Data, ArrayList.Position>{
	static class Position extends WrapperInt{

		public Position() {
			super();
		}

		public Position(int value) {
			super(value);
		}
		
	}
	private Data[] elements;
	private int lastPos=-1;
	
	public ArrayList() {
		elements=getNewSpace(20);
	}
	public ArrayList(AbstractList<Data,?> list) {
		this(list.size());
		for(Data data:list) {
			add(data);
		}
	}
	public ArrayList(int initialCapacity) {
		elements=getNewSpace(initialCapacity);
	}
	@SuppressWarnings("unchecked")
	private Data[] getNewSpace(int length){
		return (Data[]) new Object[length];
	}
	@Override
	public Position first() {
		return new Position(0);
	}
	@Override
	public Position end() {
		return new Position(lastPos+1);
	}
	@Override
	public Position next(Position p) {
		return new Position(p.value+1);
	}
	@Override
	public Position prev(Position p) {
		return new Position(p.value-1);
	}
	@Override
	public boolean add(Data data, Position p) {
		int i;
		int posToInsert=p.value;
		if(posToInsert<0 || posToInsert>size()) {
			return false;
		}
		if(isFull()) {
			Data[] aux=getNewSpace(elements.length*2);
			i=0;
			for(Data d:elements) {
				aux[i++]=d;
			}
			elements=aux;
		}
		for(i=lastPos;i>=posToInsert;i--) {
			elements[i+1]=elements[i];
		}
		elements[posToInsert]=data;
		lastPos++;
		return true;
	}
	@Override
	public Data remove(Position pos) {
		int i;
		int posToRemove=pos.value;
		Data removed;
		if(posToRemove<0 || posToRemove>=size()) {
			return null;
		}
		removed=retrieve(pos);
		for(i=lastPos;i>=posToRemove;i--) {
			elements[i]=elements[i+1];
		}
		return removed;
	}
	@Override
	public Data retrieve(Position p) {
		return elements[p.value];
	}
	@Override
	public boolean isFull() {
		return lastPos<=elements.length;
	}
	@Override
	public int size() {
		return lastPos+1;
	}
	public ArrayList<Data> concat(AbstractList<Data,?> list) {
		ArrayList<Data> concat=new ArrayList<Data>();
		for(Data data:this) {
			concat.add(data);
		}
		for(Data data:list) {
			concat.add(data);
		}
		return concat;
	}
	public static void main(String[] args) {
		ArrayList<Integer> ints=new ArrayList<>(5);
		int number=1; 
		for(int i=0;i<5;i++) {
			ints.add(number++,ints.first());
		}
		for(Integer i:ints) {
			System.out.println(i);
		}
		
	}
	
}
