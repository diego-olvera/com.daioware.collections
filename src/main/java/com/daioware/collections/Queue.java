package com.daioware.collections;

import java.util.Iterator;

public class Queue<Data> extends LinkedList<Data>{
	
	public Queue(Iterator<Data> iterator) {
		while(iterator.hasNext()) {
			enqueue(iterator.next());
		}
	}
	public void enqueue(Data d) {
		add(d);
	}
	public Data dequeue() {
		return remove(first());
	}
	public Data front() {
		return retrieve(first());
	}
}
