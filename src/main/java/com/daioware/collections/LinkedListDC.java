package com.daioware.collections;

public class LinkedListDC<Data> implements AbstractList<Data, LinkedListDC.Position> {
	
	static class Position{
		Position prev;
		Position next;
		Object data;
		
		public Position() {
		}

		public Position(Position prev, Position next, Object data) {
			this.prev = prev;
			this.next = next;
			this.data = data;
		}
	}
	
	private Position anchor;
	private int nodeCounter;
	
	public LinkedListDC() {
		anchor=new Position();
		anchor.next=anchor.prev=anchor;
		nodeCounter++;
	}

	@Override
	public Position first() {
		return anchor;
	}

	@Override
	public Position end() {
		Position p=first();
		while(p.next!=anchor) {
			p=next(p);
		}
		return p;
	}

	@Override
	public Position next(Position p) {
		return p.next;
	}

	@Override
	public Position prev(Position p) {
		return p.prev;
	}

	@Override
	public boolean add(Data data, Position p) {
		Position newItem=new Position(p, p.next, data);
		Position aux=p.next;
		p.next=newItem;
		aux.prev=newItem;
		nodeCounter++;
		return true;
	}

	@Override
	public Data remove(Position pos) {
		Data removed=retrieve(pos);
		pos.next=pos.next.next;
		nodeCounter--;
		return removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Data retrieve(Position p) {
		return (Data) p.next.data;
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public int size() {
		return nodeCounter-1;
	}
	public static void main(String[] args) {
		AbstractList<Integer,LinkedListDC.Position> ints=new LinkedListDC<Integer>();
		AbstractList<Integer,ArrayList.Position> list2=new ArrayList<Integer>();
		ArrayList<Integer> concat;
		int number=1; 
		for(int i=0;i<5;i++) {
			ints.add(number++,ints.first());
		}
		ints.add(3,ints.first());
		ints.add(3,ints.first());
		ints.add(100,ints.indexOf(5));
		list2.add(80);
		list2.add(81);
		System.out.println("Before purging");
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("After purging");
		//ints.purge();
		ints.purge((x,y)->x.compareTo(y));
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("concat");
		
		//concat=new ArrayList<Integer>(ints.concat(list2));
		
		concat=new ArrayList<Integer>(list2.concat(list2));
		//concat=(ArrayList<Integer>) list2.concat(ints);

		for(Integer i:concat) {
			System.out.println("concat:"+i);
		}
		System.out.println("size ints:"+ints.size());
	}
}
