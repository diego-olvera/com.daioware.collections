package com.daioware.collections;

import java.util.Comparator;
import java.util.function.Consumer;

public class BinaryTree<Data> {
	
	protected static class Node{
		Node right;
		Node left;
		Node father;
		Object data;
		int height;
		public Node(Object data) {
			this.data = data;
		}
		public boolean isLeaf() {
			return right==null && left==null;
		}
		public void updateHeight() {
			height=Math.max(left!=null?left.height:0,right!=null?right.height:0)+1;
		}
		public int leftChilds() {
			if(left==null) {
				return 0;
			}
			Node aux=left;
			int childs=0;
			while(aux.left!=null) {
				childs++;
			}
			return childs;
		}
		public int rightChilds() {
			if(right==null) {
				return 0;
			}
			Node aux=right;
			int childs=0;
			while(aux.right!=null) {
				childs++;
			}
			return childs;
		}
		public Node minValue() {
			return (Node) minNode().data;
		}
		public Node minNode() {
			if(left==null) {
				return this;
			}
			Node aux=left;
			while(aux.left!=null) {
				aux=aux.left;
			}
			return aux;
		}
		public Node maxValue() {
			return (Node) maxValue().data;
		}
		public Node maxNode() {
			if(right==null) {
				return this;
			}
			Node aux=right;
			while(aux.right!=null) {
				aux=aux.right;
			}
			return aux;
		}
		public String toString() {
			return data.toString();
		}
	}
	
	protected Node root;
	private Comparator<Data> comparator;
	private Consumer<Data> consumer;
	
	public BinaryTree(Comparator<Data> comparator,Consumer<Data> consumer) {
		setComparator(comparator);
		setConsumer(consumer);
	}
	public boolean add(Data data) {
		root=add(data,root);
		return true;
	}
	@SuppressWarnings("unchecked")
	protected Node add(Data data,Node node) {
		if(node==null) {
			return new Node(data);
		}
		else if(comparator.compare(data,(Data)node.data)>=0) {
			node.right=add(data,node.right);
			node.updateHeight();
			node.right.father=node;
		}
		else if(comparator.compare(data,(Data)node.data)<0) {
			node.left=add(data,node.left);
			node.updateHeight();
			node.left.father=node;
		}
		return node;
	}
	
	@SuppressWarnings("unchecked")
	public Node searchNode(Data searchItem) {
		Node current=root;
		int compRes;
		while(current!=null) {
			compRes=comparator.compare(searchItem,(Data)current.data);
			if(compRes==0) {
				return current;
			}
			else {
				current=compRes>=0?current.right:current.left;
			}
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	protected Data search(Data searchItem) {
		Node found=searchNode(searchItem);
		return found!=null?(Data)found.data:null;
	}
	public boolean isEmpty() {
		return root==null;
	}
	public void routeInOrder() {
		routeInOrderPrivate(root);
	}
	public void routeInOrder(Consumer<Data> consumer){
		Consumer<Data> prev=getConsumer();
		setConsumer(consumer);
		routeInOrderPrivate(root);
		setConsumer(prev);
	}  
	@SuppressWarnings("unchecked")
	private void routeInOrderPrivate(Node node){
		if(node!=null){
			routeInOrderPrivate(node.left);
			consumer.accept((Data) node.data);
			routeInOrderPrivate(node.right);
		}
	}
	public void routeInPreOrder() {
		routeInPreOrderPrivate(root);
	}
	public void routeInPreOrder(Consumer<Data> consumer){
		Consumer<Data> prev=getConsumer();
		setConsumer(consumer);
		routeInPreOrderPrivate(root);
		setConsumer(prev);
	}  
	@SuppressWarnings("unchecked")
	private void routeInPreOrderPrivate(Node node){
		if(node!=null){
			consumer.accept((Data) node.data);
			routeInOrderPrivate(node.left);
			routeInOrderPrivate(node.right);
		}
	}
	public void routeInPostOrder() {
		routeInPostOrderPrivate(root);
	}
	public void routeInPostOrder(Consumer<Data> consumer){
		Consumer<Data> prev=getConsumer();
		setConsumer(consumer);
		routeInPostOrderPrivate(root);
		setConsumer(prev);
	}  
	@SuppressWarnings("unchecked")
	private void routeInPostOrderPrivate(Node node){
		if(node!=null){
			routeInOrderPrivate(node.left);
			routeInOrderPrivate(node.right);
			consumer.accept((Data) node.data);
		}
	}
	public Comparator<Data> getComparator() {
		return comparator;
	}

	public void setComparator(Comparator<Data> comparator) {
		this.comparator = comparator;
	}
	public Consumer<Data> getConsumer() {
		return consumer;
	}
	public void setConsumer(Consumer<Data> consumer) {
		this.consumer = consumer;
	}
	@SuppressWarnings("unchecked")
	public Data remove(Data data) {
		Node nodeToRemove=searchNode(data);
		Data removed;
		Node minNode;
		if(nodeToRemove!=null) {
			removed=(Data) nodeToRemove.data;
			if(nodeToRemove.isLeaf()) {
				if(nodeToRemove.father==null) {
					//it means its a root and its the only item
					root=null;
				}
				else if(nodeToRemove.father.left==nodeToRemove) {
					nodeToRemove.father.left=null;
				}
				else {
					nodeToRemove.father.right=null;
				}
			}
			else if(nodeToRemove.right!=null){
				//setting min node
				minNode=nodeToRemove.right.minNode();
				//cleaning minNode from minNode's father
				if(minNode.father.left==minNode) {
					minNode.father.left=null;
				}
				else {
					minNode.father.right=null;
				}
				//checking root equalization
				if(nodeToRemove==root) {
					root=minNode;
				}
				minNode.left=nodeToRemove.left;
				//updating right node
				if(nodeToRemove.right!=null && nodeToRemove.right!=minNode) {
					minNode.right=nodeToRemove.right;
					nodeToRemove.right.father=minNode;
				}
				minNode.father=nodeToRemove.father;				
				//updating childs of the father of the removed node
				if(nodeToRemove.father!=null) {
					if(nodeToRemove.father.left==nodeToRemove) {
						nodeToRemove.father.left=minNode;
					}
					else {
						nodeToRemove.father.right=minNode;
					}
				}
				//updating childs of the removed node
				if(nodeToRemove.left!=null)nodeToRemove.left.father=minNode;
				if(nodeToRemove.right!=null)nodeToRemove.right.father=minNode;
			}
			else {
				if(nodeToRemove.left.father!=null) {
					nodeToRemove.left.father=nodeToRemove.father;
					if(nodeToRemove.father.left==nodeToRemove) {
						nodeToRemove.father.left=nodeToRemove.left;
					}
					else {
						nodeToRemove.father.right=nodeToRemove.right;
					}
				}
			}
		}
		else {
			removed=null;
		}
		return removed;
	}
	public static void main(String[] args) {
		int numberToRemove=25;
		Comparator<Integer> comp=new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return Integer.compare(o1, o2);
			}
		};
		Consumer<Integer> consumer=new Consumer<Integer>() {
			@Override
			public void accept(Integer t) {
				System.out.print(t+",");
			}
		};
		BinaryTree<Integer> tree=new BinaryTree<>(comp,consumer);
		tree.add(20);
		tree.add(15);
		tree.add(30);
		tree.add(25);
		tree.add(19);
		tree.add(10);
		tree.add(12);
		tree.add(11);
		tree.add(13);
		tree.add(5);
		tree.add(9);
		tree.add(3);
		tree.add(23);
		tree.add(27);
		tree.add(35);
		tree.add(32);
		tree.add(37);
		System.out.println("------Order-----");
		tree.routeInOrder();
		for(numberToRemove=3;numberToRemove<=38;numberToRemove++) {
			System.out.println("----Removing "+numberToRemove+" ----");
			System.out.println("Removed:"+tree.remove(numberToRemove));
			System.out.println("------Order------");
			tree.routeInOrder();
			//Util.pause();
		}
	}	
}
