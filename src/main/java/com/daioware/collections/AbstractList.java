package com.daioware.collections;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public interface AbstractList<Data,Position> extends Iterable<Data>{
	Position first();
	Position end();
	default Position last() {
		return prev(end());
	}
	Position next(Position p);
	Position prev(Position p);
	boolean add(Data data,Position p);
	default boolean add(Data data,int index) {
		return add(data,intToPos(index));
	}
	default boolean add(Data data) {
		return add(data,end()); 
	}
	default Data remove(int index) {
		return remove(intToPos(index));
	}
	Data remove(Position pos);
	default boolean removeData(Data data) {
		Position p=indexOf(data);
		return p.equals(end())?remove(p)!=null:null;
	}
	default Position intToPos(int intPos) {
		Position p,q;
		int i=0;
		for(p=first(),q=last();!p.equals(q) && i<intPos;p=next(p),i++);
		return p;	
	}
	default int posToInt(Position p) {
		int i=0;
		Position aux=first();
		while(aux!=p) {
			aux=next(p);
			i++;
		}
		return i;
	}
	default Data get(int p) {
		return retrieve(intToPos(p));
	}
	Data retrieve(Position p);
	
	default boolean isEmpty() {
		return first().equals(end());
	}
	boolean isFull();
	int size();
	
	default Position indexOf(Data search) {
		return indexOf(search,first());
	}
	default Position indexOf(Data search,Position beginningPos) {
		Position p,q;
		for(p=beginningPos,q=end();!p.equals(q);p=next(p)) {
			if(retrieve(p).equals(search)) {
				return p;
			}
		}
		return p;
	}
	default int indexOf(Data search,int beginningPos) {
		Position p,q;
		for(p=intToPos(beginningPos),q=end();!p.equals(q);p=next(p)) {
			if(retrieve(p).equals(search)) {
				return posToInt(p);
			}
		}
		return posToInt(p);
	}
	default boolean contains(Data search) {
		return indexOf(search)!=end();
	}
	default Position indexOf(Comparator<Data> comparator,Data search,Position beginningPos) {
		Position p,q;
		for(p=first(),q=end();!p.equals(q);p=next(p)) {
			if(comparator.compare(retrieve(p), search)==0) {
				return p;
			}
		}
		return p;	
	}

	default Position indexOf(Comparator<Data> comparator,Data search) {
		return indexOf(comparator, search,first());
	}
	default AbstractList<Data,?> concat(AbstractList<Data,?> list) {
		LinkedList<Data> concat=new LinkedList<Data>();
		for(Data data:this) {
			concat.add(data);
		}
		for(Data data:list) {
			concat.add(data);
		}
		return concat;
	}
	default void purge() {
		Position p,q;
		Data dataP;
		for(p=first();!p.equals(end());p=next(p)) {
			q=next(p);
			dataP=retrieve(p);
			while(!q.equals(end())) {
				if(dataP.equals(retrieve(q))) {
					remove(q);
				}
				else {
					q=next(q);
				}
			}
		}
	}
	default void purge(Comparator<Data> comp) {
		Position p,q;
		Data dataP;
		for(p=first();!p.equals(end());p=next(p)) {
			q=next(p);
			dataP=retrieve(p);
			while(!q.equals(end())) {
				if(comp.compare(dataP, retrieve(q))==0) {
					remove(q);
				}
				else {
					q=next(q);
				}
			}
		}
	}

	default Iterator<Data> iterator(){
		AbstractList<Data, Position> thisP=this;
		return new Iterator<Data>() {
				Position current=first(),end=end();
				@Override
				public boolean hasNext() {
					return !current.equals(end);
				}

				@Override
				public Data next() {
					Data data=retrieve(current);
					current=thisP.next(current);
					return data;
				}
		};
	}
	@SuppressWarnings("unchecked")
	default public Object[] toArray() {
		Data[] elements=(Data[]) new Object[size()];
		int i=0;
		for(Data data:this) {
			elements[i++]=data;
		}
		return elements;
	}
	default public void addAll(AbstractList<Data,?> list) {
		list.forEach((e)->add(e));
	}
	default public void addAll(List<Data> list) {
		list.forEach((e)->add(e));
	}
	default public void clear() {
		while(!isEmpty()) {
			remove(first());
		}
	}
}
