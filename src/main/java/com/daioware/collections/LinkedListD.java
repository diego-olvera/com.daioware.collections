package com.daioware.collections;

public class LinkedListD<Data> implements AbstractList<Data,LinkedListD.Position>{
	
	static class Position{
		Position prev;
		Position next;
		Object data;
		public Position(Position prev, Position next, Object data) {
			this.prev = prev;
			this.next = next;
			this.data = data;
		}	
	}
	private Position anchor;
	private int nodeCounter;
	
	public LinkedListD() {
		anchor=new Position(null, null, null);
		nodeCounter++;
	}

	@Override
	public Position first() {
		return anchor;
	}

	@Override
	public Position end() {
		Position p=first();
		while(p.next!=null) {
			p=next(p);
		}
		return p;
	}

	@Override
	public Position next(Position p) {
		return p.next;
	}

	@Override
	public Position prev(Position p) {
		return p.prev;
	}

	@Override
	public boolean add(Data data, Position p) {
		p.next=new Position(p,p.next, data);
		nodeCounter++;
		return true;
	}

	@Override
	public Data remove(Position pos) {
		Data removed=retrieve(pos);
		Position aux=pos.next;
		pos.next=aux.next;
		if(aux.next!=null) {
			aux.next.prev=pos;
		}
		nodeCounter--;
		return removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Data retrieve(Position p) {
		return (Data) p.next.data;
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public int size() {
		return nodeCounter-1;
	}
	public static void main(String[] args) {
		AbstractList<Integer,LinkedListD.Position> ints=new LinkedListD<Integer>();
		AbstractList<Integer,ArrayList.Position> list2=new ArrayList<Integer>();
		ArrayList<Integer> concat;
		int number=1; 
		for(int i=0;i<5;i++) {
			ints.add(number++,ints.first());
		}
		ints.add(3,ints.first());
		ints.add(3,ints.first());
		list2.add(80);
		list2.add(81);
		System.out.println("Before purging");
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("After purging");
		//ints.purge();
		ints.purge((x,y)->x.compareTo(y));
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("concat");
		
		//concat=new ArrayList<Integer>(ints.concat(list2));
		
		concat=new ArrayList<Integer>(list2.concat(list2));
		//concat=(ArrayList<Integer>) list2.concat(ints);

		for(Integer i:concat) {
			System.out.println("concat:"+i);
		}
	}
}
