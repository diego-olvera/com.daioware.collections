package com.daioware.collections;


import java.util.Comparator;
import java.util.function.Consumer;

class ArbolAVL_Excepcion extends Exception{
	private static final long serialVersionUID = 1L;
	public ArbolAVL_Excepcion(String mensaje){
		super(mensaje);
	}
	
}
class NodoAVL<T> {  
	NodoAVL<T> hijoIzquierdo;
	NodoAVL<T> hijoDerecho; 
    T dato;
    long altura; 
    public NodoAVL() {}
    public NodoAVL(T e){
        dato = e;
    }     
}
public class SbTree<T> {
	private NodoAVL<T> raiz;
	private long cuentaNodos;
	private Comparator<T> comparador;
	private Consumer<T> consumer;
	public SbTree(Comparator<T> comparador,Consumer<T> objectFunction){
		setComparador(comparador);
		consumer=objectFunction;
	}
	public boolean setObjetoFuncion(Consumer<T> o){
		if(o!=null){
			consumer=o;
			return true;
		}
		else{
			return false;
		}
	}
	public boolean setComparador(Comparator<T> c){
		if(c!=null){
			comparador=c;
			return true;
		}
		else{
			return false;
		}
	}
	public boolean estaVacio() {
		return raiz == null;
	}
	
	/* En la l�gica del programa el arbol queda vac�o */
	public void vaciarArbol() {
		cuentaNodos = 0;
		raiz = null;//con esto se pierde el acceso a todos los nodos
	}
	
	public boolean inserta(T data) throws ArbolAVL_Excepcion {
		raiz = inserta(data, raiz);
		return true;
	}
	
	public long dameAltura(){
		return dameAltura(raiz);
	}
	
	public long dameAltura(NodoAVL<T> nodo) {
		return nodo == null ? -1 : nodo.altura;
	}
	
	public long dameCuentaNodos(){
		return cuentaNodos;
	}
	
	private NodoAVL<T> inserta(T x, NodoAVL<T> nodo) throws ArbolAVL_Excepcion{
		int comp;
		if (nodo == null) {
			nodo = new NodoAVL<T>(x);
			cuentaNodos++;
		} 
		else if ((comp=comparador.compare(x,nodo.dato)) < 0) {
			nodo.hijoIzquierdo = inserta(x, nodo.hijoIzquierdo);
			if (dameAltura(nodo.hijoIzquierdo) - dameAltura(nodo.hijoDerecho) == 2)
				if (comparador.compare(x,nodo.hijoIzquierdo.dato) < 0)
					nodo = rotarConHijoIzquierdo(nodo);
				else
					nodo = rotacionDobleHijoIzquierdo(nodo);
		} 
		else if(comp>0){
			nodo.hijoDerecho = inserta(x, nodo.hijoDerecho);
			if (dameAltura(nodo.hijoDerecho) - dameAltura(nodo.hijoIzquierdo) == 2)
				if (comparador.compare(x,nodo.hijoDerecho.dato) > 0)
					nodo = rotarConHijoDerecho(nodo);
				else
					nodo = rotacionDobleHijoDerecho(nodo);
		}
		else{
			throw new ArbolAVL_Excepcion("Repeated "+x);
		}
		nodo.altura = Math.max(dameAltura(nodo.hijoIzquierdo),
				dameAltura(nodo.hijoDerecho)) + 1;
		return nodo;
	}
	
	private NodoAVL<T> rotarConHijoIzquierdo(NodoAVL<T> k2) {
		NodoAVL<T> k1 = k2.hijoIzquierdo;
		k2.hijoIzquierdo = k1.hijoDerecho;
		k1.hijoDerecho = k2;
		k2.altura = Math.max(dameAltura(k2.hijoIzquierdo),
				dameAltura(k2.hijoDerecho)) + 1;
		k1.altura = Math.max(dameAltura(k1.hijoIzquierdo), k2.altura) + 1;
		return k1;
	}

	private NodoAVL<T> rotarConHijoDerecho(NodoAVL<T> k1) {
		NodoAVL<T> k2 = k1.hijoDerecho;
		k1.hijoDerecho = k2.hijoIzquierdo;
		k2.hijoIzquierdo = k1;
		k1.altura = Math.max(dameAltura(k1.hijoIzquierdo),
				dameAltura(k1.hijoDerecho)) + 1;
		k2.altura = Math.max(dameAltura(k2.hijoDerecho), k1.altura) + 1;
		return k2;
	}
	
	private NodoAVL<T> rotacionDobleHijoIzquierdo(NodoAVL<T> k3) {
		k3.hijoIzquierdo = rotarConHijoDerecho(k3.hijoIzquierdo);
		return rotarConHijoIzquierdo(k3);
	}
	
	private NodoAVL<T> rotacionDobleHijoDerecho(NodoAVL<T> k1) {
		k1.hijoDerecho = rotarConHijoIzquierdo(k1.hijoDerecho);
		return rotarConHijoDerecho(k1);
	}
	
	public long dameAlturaHijoIzquierdoDeRaiz(){
		return dameAltura(raiz.hijoIzquierdo);
	}
	
	public long dameAlturaHijoDerechoDeRaiz(){
		return dameAltura(raiz.hijoDerecho);
	}
	
	public boolean contiene(T x) {
		return buscar(x) != null;
	}
	public NodoAVL<T> buscar(T x) {
		NodoAVL<T> actual = raiz;
		while (actual != null) {   
			int comp=comparador.compare(x,actual.dato);
			if(comp==0){
				consumer.accept(actual.dato);
				return actual;
			}
			else if(comp<0){
				actual = actual.hijoIzquierdo;
			}
			else{
				actual = actual.hijoDerecho;
			} 
		}
		return actual;
	}

	public void imprimirEnOrden() {
		imprimirEnOrden(raiz);
	}
	
	private void imprimirEnOrden(NodoAVL<T> nodo) {
		if (nodo!=null) {
			imprimirEnOrden(nodo.hijoIzquierdo);
			consumer.accept(nodo.dato);
			imprimirEnOrden(nodo.hijoDerecho);
		}
	}
	
	public void imprimirPreorden() {
		imprimirPreorden(raiz);
	}

	private void imprimirPreorden(NodoAVL<T> nodo) {
		if (nodo != null) {
			consumer.accept(nodo.dato);
			imprimirPreorden(nodo.hijoIzquierdo);
			imprimirPreorden(nodo.hijoDerecho);
		}
	}

	public void imprimirPostorden() {
		imprimirPostorden(raiz);
	}
	
	private void imprimirPostorden(NodoAVL<T> nodo) {
		if (nodo != null) {
			imprimirPostorden(nodo.hijoIzquierdo);
			imprimirPostorden(nodo.hijoDerecho);
			consumer.accept(nodo.dato);
		}
	}
	public static void main(String[] args) throws ArbolAVL_Excepcion{
		Comparator<Integer> comparadorEnteros=new Comparator<Integer>() {

			@Override
			public int compare(Integer x, Integer y) {
				return Integer.compare(x, y);
			}
		}; 
		Consumer<Integer> objetoFuncion=new Consumer<Integer>() {				
			@Override
			public void accept(Integer e) {
				System.out.println("ObjFunc --Dato:"+e);				
			} 
		};
		SbTree<Integer> arbolEnteros=new SbTree<Integer>(comparadorEnteros,objetoFuncion);
		arbolEnteros.inserta(new Integer(10));
		arbolEnteros.inserta(new Integer(1));
		arbolEnteros.inserta(new Integer(9));
		arbolEnteros.inserta(new Integer(2));
		arbolEnteros.inserta(new Integer(7));
		arbolEnteros.inserta(new Integer(7));
		arbolEnteros.inserta(new Integer(7));
		arbolEnteros.inserta(new Integer(5));
		arbolEnteros.inserta(new Integer(6));
		arbolEnteros.inserta(new Integer(5));
		arbolEnteros.inserta(new Integer(5));
		
		
		arbolEnteros.imprimirEnOrden();
		
		System.out.println("Altura:"+arbolEnteros.dameAltura());
		//Integer n=new Integer(6);
		//System.out.println("Buscar el "+n+": "+arbolEnteros.buscarElemento(n));
		System.out.println("Ahora a buscar el 6 en todo el arbol:");
		arbolEnteros.buscar(new Integer(6));
		//Util.printArray(resultados.toArray());
		
	}
	
}
