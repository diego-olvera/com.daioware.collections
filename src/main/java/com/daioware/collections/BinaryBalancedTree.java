package com.daioware.collections;

import java.util.Comparator;
import java.util.function.Consumer;

import com.daioware.commons.wrapper.WrapperBoolean;

public class BinaryBalancedTree<Data> extends BinaryTree<Data> {

	private int maxUnbalance;
	
	public BinaryBalancedTree(Comparator<Data> comparator, Consumer<Data> consumer) {
		this(comparator, consumer,1);
	}

	public BinaryBalancedTree(Comparator<Data> comparator, Consumer<Data> consumer, int maxUnbalance) {
		super(comparator, consumer);
		setMaxUnbalance(maxUnbalance);
	}
	
	public int getMaxUnbalance() {
		return maxUnbalance;
	}

	public void setMaxUnbalance(int maxUnbalance) {
		this.maxUnbalance = maxUnbalance;
	}

	@Override
	public boolean add(Data data) {
		boolean added=add(data);
		if(added) {
			balance(root);
		}
		return added;		
	}
	public void balance(Node node) {
		WrapperBoolean bool=new WrapperBoolean();
		Node unbalancedNode;
		if(node!=null) {
			unbalancedNode=giveMeLoosebalance(bool,node);
			if(unbalancedNode!=null) {
				if(bool.value) {
					//left is unbalanced
				}
				else {
					//right is unbalanced
				}
			}
			balance(node.left);
			balance(node.right);		
		}		
	}
	public Node giveMeLoosebalance(WrapperBoolean bool,Node node) {
		int lefts=node.leftChilds(),rights=node.rightChilds();
		int diff=Math.abs(lefts-rights);
		if(diff>getMaxUnbalance()) {
			bool.value=lefts>rights;
			return node;
		}
		else {
			return null;
		}		
	}
	public static void main(String[] args) {
		//int numberToRemove=25;
		Comparator<Integer> comp=new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return Integer.compare(o1, o2);
			}
		};
		Consumer<Integer> consumer=new Consumer<Integer>() {
			@Override
			public void accept(Integer t) {
				System.out.print(t+",");
			}
		};
		BinaryTree<Integer> tree=new BinaryTree<>(comp,consumer);
		tree.add(20);
		tree.add(15);
		tree.add(22);
		tree.add(30);
		tree.add(33);
	}
}
