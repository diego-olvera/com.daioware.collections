package com.daioware.collections;

public class LinkedList<Data> implements AbstractList<Data,LinkedList.Position> {
	
	private int nodeCounter;
	static class Position{
		Position next;
		Object data;
		Position(Position next,Object data) {
			this.next=next;
			this.data=data;
		}
	}
	private Position anchor;
	public LinkedList() {
		anchor=new Position(null,null);
		nodeCounter++;
	}
	public LinkedList(AbstractList<Data,?> list) {
		this();
		for(Data data:list) {
			add(data);
		}
	}
	@Override
	public Position first() {
		return anchor;
	}

	@Override
	public Position end() {
		Position aux=first();
		while(aux.next!=null) {
			aux=next(aux);
		}
		return aux;
	}

	@Override
	public Position next(Position p) {
		return p.next;
	}

	@Override
	public Position prev(Position p) {
		Position aux=first();
		while(aux!=null && aux.next!=null && aux.next!=p) {
			aux=next(aux);
		}
		return aux;
	}

	@Override
	public boolean add(Data data, Position p) {	
		p.next=new Position(p.next, data);
		nodeCounter++;
		return true;
	}

	@Override
	public Data remove(Position pos) {
		Data removed=retrieve(pos);
		pos.next=pos.next.next;
		nodeCounter--;
		return removed;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Data retrieve(Position p) {
		return (Data) p.next.data;
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public int size() {
		return nodeCounter-1;
	}
	public LinkedList<Data> concat(AbstractList<Data,?> list) {
		LinkedList<Data> concat=new LinkedList<Data>();
		for(Data data:this) {
			concat.add(data);
		}
		for(Data data:list) {
			concat.add(data);
		}
		return concat;
	}
	public static void main(String[] args) {
		AbstractList<Integer,LinkedList.Position> ints=new LinkedList<Integer>();
		AbstractList<Integer,ArrayList.Position> list2=new ArrayList<Integer>();
		ArrayList<Integer> concat;
		int number=1; 
		for(int i=0;i<5;i++) {
			ints.add(number++,ints.first());
		}
		ints.add(3,ints.first());
		ints.add(3,ints.first());
		list2.add(80);
		list2.add(81);
		System.out.println("Before purging");
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("After purging");
		//ints.purge();
		ints.purge((x,y)->x.compareTo(y));
		for(Integer i:ints) {
			System.out.println(i);
		}
		System.out.println("concat");
		
		//concat=new ArrayList<Integer>(ints.concat(list2));
		
		concat=new ArrayList<Integer>(list2.concat(list2));
		//concat=(ArrayList<Integer>) list2.concat(ints);

		for(Integer i:concat) {
			System.out.println("concat:"+i);
		}
	}

}
