package com.daioware.collections.watcher;

import java.util.Collection;
import java.util.Comparator;

public interface WatcherCollection<T> {
	void initialize();
	void finalize();
	default void runChanges() {
		synchronized (this) {
			notify();
		}
	}
	default void afterAdd(int index, T element) {
		runChanges();
	}
	default void afterAdd(T element) {
		runChanges();
	}
	default void afterAddAll(Collection<? extends T> c) {
		runChanges();
	}
	default void afterAddAll(int index,Collection<? extends T> c) {
		runChanges();
	}
	default void afterClear() {
		runChanges();
	}
	default void afterRemove(int index) {
		runChanges();
	}
	default void afterRemove(Object e) {
		runChanges();
	}
	default void afterRemoveAll() {
		runChanges();
	}
	default void afterReplaceAll() {
		runChanges();
	}
	default void afterSet(int index, T element) {
		runChanges();
	}
	default void afterSort(Comparator<? super T> c) {
		runChanges();
	}
}
