package com.daioware.collections.watcher;

import java.util.HashMap;
import java.util.Map;

public class WatcherHashMap<Key,Value> extends HashMap<Key,Value> implements Runnable,WatcherCollection<Value>{
	private static final long serialVersionUID = 1L;
	private boolean active=true;
	private Runnable target;
	
	public WatcherHashMap(Runnable target) {
		super();
		setTarget(target);
		initialize();
		new Thread(this).start();	
	}

	public WatcherHashMap(int initialCapacity, float loadFactor,Runnable target) {
		super(initialCapacity, loadFactor);
		setTarget(target);
		initialize();
		new Thread(this).start();	
	}

	public WatcherHashMap(int initialCapacity,Runnable target) {
		super(initialCapacity);
		setTarget(target);
		initialize();
		new Thread(this).start();	
	}

	public WatcherHashMap(Map<? extends Key, ? extends Value> m) {
		super(m);
		setTarget(target);
		initialize();
		applicateChanges();
		new Thread(this).start();	
	}

	@Override
	public void initialize() {
		
	}
	public synchronized void finalize() {
		setActive(false);
	}
	public Runnable getTarget() {
		return target;
	}
	public void setTarget(Runnable target) {
		this.target = target;
	}
	public boolean isActive() {
		return active;
	}
	protected synchronized void setActive(boolean active) {
		this.active = active;
		if(!active) {
			notify();
		}
	}
	
	@Override
	public Value put(Key key, Value value) {
		Value v=super.put(key, value);
		afterAdd(v);
		return v;
	}

	@Override
	public Value remove(Object key) {
		Value v=super.remove(key);
		if(v!=null) {
			afterRemove(v);
		}
		return v;
	}

	public void applicateChanges() {
		new Thread(getTarget()).start();
	}
	public synchronized void run() {
		while(isActive()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(isActive()) {
				applicateChanges();
			}
		}
	}
	public static void main(String[] args) throws InterruptedException {
		Runnable run=new Runnable() {
			@Override
			public void run() {
				System.out.println("Updated");				
			}
		};
		WatcherHashMap<String,String> integers=new WatcherHashMap<>(run);
		for(int i=0;i<10;i++) {
			integers.put(String.valueOf(i),String.valueOf(i+1));
			Thread.sleep(1000);
		}
		for(String s:integers.values()) {
			System.out.println(s);
		}
		integers.finalize();
	}

	
}
