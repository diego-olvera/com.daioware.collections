package com.daioware.collections.watcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.UnaryOperator;

public class WatcherArrayList<T> extends ArrayList<T> implements Runnable,WatcherCollection<T>{
	private static final long serialVersionUID = 1L;
	private boolean active=true;
	private Runnable target;
	
	public WatcherArrayList(Collection<? extends T> coll,Runnable target) {
		super(coll);
		setTarget(target);
		initialize();
		applicateChanges();
		new Thread(this).start();
	}

	public WatcherArrayList(int size,Runnable target) {
		super(size);
		setTarget(target);
		initialize();
		new Thread(this).start();
	}

	public WatcherArrayList(Runnable target) {
		super();
		setTarget(target);
		initialize();
		new Thread(this).start();

	}
	@Override
	public void initialize() {
		
	}
	public synchronized void finalize() {
		setActive(false);
	}
	public Runnable getTarget() {
		return target;
	}
	public void setTarget(Runnable target) {
		this.target = target;
	}
	public boolean isActive() {
		return active;
	}
	protected synchronized void setActive(boolean active) {
		this.active = active;
		if(!active) {
			notify();
		}
	}
	@Override
	public void add(int index, T element) {
		super.add(index, element);
		afterAdd(index,element);
	}

	@Override
	public boolean add(T e) {
		boolean b= super.add(e);
		if(b)afterAdd(e);
		return b;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean added=super.addAll(c);
		if(added) {
			afterAddAll(c);
		}
		return added;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		boolean added=super.addAll(index,c);
		if(added) {
			afterAddAll(index,c);
		}
		return added;
	}

	@Override
	public void clear() {
		super.clear();
		runChanges();
	}

	@Override
	public T remove(int index) {
		T e= super.remove(index);
		if(e!=null)afterRemove(index);
		return e;
	}

	@Override
	public boolean remove(Object o) {
		boolean b=super.remove(o);
		if(b)afterRemove(o);
		return b;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean b=super.removeAll(c);
		if(b)afterRemoveAll();
		return b;
	}

	@Override
	public void replaceAll(UnaryOperator<T> operator) {
		super.replaceAll(operator);
		afterReplaceAll();
	}

	@Override
	public T set(int index, T element) {
		T e= super.set(index, element);
		afterSet(index,element);
		return e;
	}
	
	@Override
	public void sort(Comparator<? super T> c) {
		super.sort(c);
		runChanges();
	}

	public void applicateChanges() {
		new Thread(getTarget()).start();
	}
	public synchronized void run() {
		while(isActive()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(isActive()) {
				applicateChanges();
			}
		}
	}
	public static void main(String[] args) throws InterruptedException {
		Runnable run=new Runnable() {
			@Override
			public void run() {
				System.out.println("Updated");				
			}
		};
		WatcherArrayList<Integer> integers=new WatcherArrayList<>(run);
		for(int i=0;i<10;i++) {
			integers.add(i);
			Thread.sleep(1000);
		}
		integers.finalize();
	}

	
}
