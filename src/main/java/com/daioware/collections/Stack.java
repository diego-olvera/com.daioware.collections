package com.daioware.collections;

public class Stack<Data> extends LinkedList<Data>{
	
	public void push(Data data) {
		add(data,first());
	}
	public Data pop() {
		return remove(first());
	}
	public Data peek() {
		return retrieve(first());
	}
}
