package com.daioware.collections.util;

import java.util.Comparator;
import java.util.List;

public final class Searchs {
	public static<T> int binarySearch(List<T> elements,Comparator<T> comp,T key) {
		int lo = 0;
        int hi =elements.size() - 1;
        T midE;
        int mid;
        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            midE=elements.get(mid);
            if ( comp.compare(key, midE)<0) {
            	hi = mid - 1;
            }
            else if (comp.compare(key, midE)>0) {
            	lo = mid + 1;
            }
            else{
            	return mid;
            }
        }
        return -1;
	}
	public static<T> int binarySearch(T[] elements,Comparator<T> comp,T key) {
		int lo = 0;
        int hi =elements.length - 1;
        T midE;
        int mid;
        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            midE=elements[mid];
            if ( comp.compare(key, midE)<0) {
            	hi = mid - 1;
            }
            else if (comp.compare(key, midE)>0) {
            	lo = mid + 1;
            }
            else{
            	return mid;
            }
        }
        return -1;
	}
}
